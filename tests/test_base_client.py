import inspect
import logging
from typing import TYPE_CHECKING

from mock.mock import MagicMock



def test_base_url_none(base_client: 'BaseClient'):
    assert base_client
    assert base_client.base_url is None


def test_base_url_env_var(base_client_base_url_env_var: 'BaseClient', base_url: str):
    assert base_client_base_url_env_var
    assert base_client_base_url_env_var.base_url == base_url


def test_base_url(base_client_base_url: 'BaseClient', base_url: str):
    assert base_client_base_url
    assert base_client_base_url.base_url == base_url


def test_method_generation(base_client: 'BaseClient'):
    assert base_client
    for method in base_client.method_url_binding:
        assert method.name in dir(base_client)
        if method.docstring:
            assert method.docstring == getattr(base_client, method.name).__doc__
        assert method.resource_name in inspect.signature(getattr(base_client, method.name)).parameters.keys()


def test_method_response_key(base_client: 'BaseClient'):
    assert base_client
    response = base_client.get_resource_with_response_key(response_key='123')  # type: ignore[attr-defined]
    assert len(response) == 1


def test_method_logging_level(base_client: 'BaseClient'):
    base_client._logger = MagicMock()
    assert base_client
    for log_level in [logging.DEBUG, logging.INFO, logging.WARNING, logging.ERROR, logging.CRITICAL]:
        _ = base_client.get_resource(resource='123', logging_level=log_level)  # type: ignore[attr-defined]
        assert base_client._logger.log.called == 1
        assert base_client._logger.log.call_args[0][0] == log_level
