import os

import pytest

from skcr_utils.base_client import BaseClient
from tests.mocks.client import BaseClientMock


def pytest_configure():
    pass


@pytest.fixture(scope='session')
def base_url() -> str:
    return 'http://mock/base/url'


@pytest.fixture(scope='session')
def base_client():
    return BaseClientMock()


@pytest.fixture(scope='session')
def base_client_base_url_env_var(base_url: str) -> BaseClient:
    os.environ['CLIENT_BASE_URL'] = base_url
    return BaseClientMock()


@pytest.fixture(scope='session')
def base_client_base_url(base_url: str) -> BaseClient:
    return BaseClientMock(base_url=base_url)
