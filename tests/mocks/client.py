from pathlib import Path
from typing import List

from requests import Response

from skcr_utils.base_client import BaseClient
from skcr_utils.base_client.etc import MethodDetails

BASE_URL = ''
MOCK_DATA_PATH = Path('tests/mocks/response.json')


class BaseClientMock(BaseClient):
    _pkg_name = 'Mock'

    @property
    def method_url_binding(self) -> List[MethodDetails]:
        return [
            MethodDetails(
                name='get_resource',
                endpoint='/resource',
                resource_name='resource',
                docstring='get resource docstring',
            ),
            MethodDetails(
                name='get_resource_with_response_key',
                endpoint='/endpoint_response_key',
                resource_name='response_key',
                docstring='get resource with response_key docstring',
                response_key='data',
            ),
        ]

    def _build_final_url(self, url: str, **kwargs) -> str:
        return url

    def _get_response(self, url: str) -> Response:
        resp = Response()
        resp.status_code = 200
        resp._content = bytes(MOCK_DATA_PATH.read_text(), 'utf-8')
        return resp
