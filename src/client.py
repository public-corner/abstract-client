import inspect
import os
from abc import ABC, abstractmethod
from logging import Logger, getLogger
from typing import Callable, List, Optional, Union

import pkg_resources
from makefun import create_function
from requests import JSONDecodeError, Response, Session
from requests.auth import HTTPBasicAuth

__all__ = ['BaseClient']



def _freeze_args(func: Callable, id_name: str, **kwargs):
    frozen_kwargs = kwargs

    def wrapper(*args, **kwargs):
        frame = inspect.currentframe()
        passed_kwargs = inspect.getargvalues(frame).locals['kwargs']
        if 'url' in passed_kwargs.keys():
            raise ValueError('Forbidden to pass "url" as an argument and overwrite default value.')

        id_value = passed_kwargs[id_name]
        kwargs['resource_id'] = id_value
        del kwargs[id_name]

        kwargs.update(frozen_kwargs)
        return func(*args, **kwargs)

    return wrapper


class BaseClient(ABC):  # type: ignore
    _logger: Logger
    _pkg_name: str
    _parameter_store_key: Optional[str] = None
    _parameter_store_region: Optional[str] = None
    _session: Optional[Session] = None
    base_url: Optional[str] = None

    def __init__(self, *args, **kwargs):
        self.base_url = self.base_url or os.getenv('CLIENT_BASE_URL')
        self._set_dict_defined_methods()
        self._logger = getLogger(self.__class__.__name__)

    def _set_dict_defined_methods(self):
        for method in self.method_url_binding:
            url = method.endpoint
            id_name = method.resource_name
            response_key = method.response_key
            docstring = method.docstring

            setattr(
                self, method.name, _freeze_args(self._get_data, id_name=id_name, url=url, response_key=response_key)
            )
            func_name = method.name.strip('_')
            self.__dict__[func_name] = self._generate_signature(
                method.name,
                id_name=id_name,
            )

            if docstring:
                self.__dict__[func_name].__doc__ = docstring

    def _generate_signature(self, func_name, id_name):
        frame = inspect.currentframe()
        args, _, _, values = inspect.getargvalues(frame)
        func = getattr(self, func_name)
        func_name = func_name.strip('_')

        func_sig = '{}({}, **kwargs)'.format(func_name, values['id_name'])
        func_gen = create_function(func_sig, func)
        setattr(self, func_name, func_gen)
        func = getattr(self, func_name)
        return func

    @property
    def session(self) -> Session:
        if not self._session:
            try:
                version = pkg_resources.get_distribution(self._pkg_name).version
            except pkg_resources.DistributionNotFound:
                version = '1.0.0'
            self._session = Session()
            self._session.headers = {'User-Agent': f'skillcorner.{self._pkg_name}.{version}'}
            self._session.auth = self.authenticate()
        return self._session

    @staticmethod
    def authenticate() -> Union[HTTPBasicAuth, None]:
        return None

    @property
    @abstractmethod
    def method_url_binding(self) -> List[MethodDetails]:
        return list()

    def _request(self, url: str, **kwargs) -> Response:
        raise_for_status = kwargs.get('raise_for_status', True)
        logging_level = kwargs.get('logging_level')
        url = self._build_final_url(url, **kwargs)
        self._logger.info(f'Sending GET request to: {url}')
        resp = self._get_response(url=url)

        if logging_level:
            try:
                logger_response = resp.json()
            except JSONDecodeError:
                logger_response = resp.text

            self._logger.log(
                logging_level,
                f'Response from {self._pkg_name}',
                extra={'url': url, 'response_body': logger_response},
            )
        if raise_for_status:
            resp.raise_for_status()
        return resp

    def _get_response(self, url: str) -> Response:
        return self.session.get(url=url)

    @abstractmethod
    def _build_final_url(self, url: str, **kwargs) -> str:
        pass

    def _get_data(self, resource_id: str, url: str, response_key: Optional[str] = None, **kwargs) -> Response:
        url = url.format(resource_id)
        if response_key:
            return self._request(url=url, **kwargs).json()[response_key]
        else:
            return self._request(url=url, **kwargs).json()
