from dataclasses import dataclass
from typing import Optional


@dataclass
class MethodDetails:
    name: str
    endpoint: str
    resource_name: str
    docstring: str
    response_key: Optional[str] = None
